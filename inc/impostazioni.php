<div id="woo_fattureincloud">
    <header></header>
</div>

	<h2>Inserire le API del sito fattureincloud.it: API >> MOSTRA API UID E API KEY </h2>
	<hr>
	<?php

	/**
	 *
	 * Form for API Value API UID and API KEY
	 *
	 */
	?>

<table border="0" cellpadding="6">
    <tr>
            <td align="right">


            <form id="woo-fattureincloud-settings_key" method="POST">

	    	<?php wp_nonce_field(); ?>

            <label for="woo_fattureincloud_apiuid">API UID</label>

            <input type="password" name="api_uid_fattureincloud" placeholder="api uid"
                   value="<?php echo get_option('api_uid_fattureincloud'); ?>">

            <label for="woo_fattureincloud_apikey">API KEY</label>

            <input type="password" name="api_key_fattureincloud" placeholder="api key"
                   value="<?php echo get_option('api_key_fattureincloud'); ?>">

            </td>
            <td>

                    <input type="submit" value="Save" class="button button-primary button-large">
            </td>
	</form>

        </tr>
        <tr>
            <td align="right">

<form method="POST">
	<label  for="fattureincloud_auto_save"><?php echo __( 'Abilita la creazione <b>in automatico</b> della fattura su fattureincloud.it quando l\'ordine è Completato', 'woo-fattureincloud' );?></label>

	<input type="hidden" name="fattureincloud_auto_save" value="0" />
	<input type="checkbox" name="fattureincloud_auto_save" id="fattureincloud_auto_save" value="1" <?php if ( 1 == get_option('fattureincloud_auto_save') ) echo 'checked';

	else echo '';

	?>>
            </td>
            <td>
	<input type="submit" value="Save" class="button button-primary button-large">
</form>
            </td>
        </tr>


        <tr>
            <td align="right">

<form method="POST">
	<label for="fattureincloud_partiva_codfisc"><?php echo __( 'Attiva la voce Partita Iva e Codice Fiscale nel Checkout di WooFatture', 'woo-fattureincloud' );?></label>

	<input type="hidden" name="fattureincloud_partiva_codfisc" value="0" />
	<input type="checkbox" name="fattureincloud_partiva_codfisc" id="fattureincloud_partiva_codfisc" value="1" <?php if ( 1 == get_option('fattureincloud_partiva_codfisc') ) echo 'checked';

	else echo '';

	?>>
            </td>
            <td>
	<input type="submit" value="Save" class="button button-primary button-large">
</form>

            </td>
        </tr>

    <tr>
        <td>
            <b>In alternativa</b> è possibile utilizzare il plugin <i>WooCommerce P.IVA e Codice Fiscale per Italia</i>

        </td>

    </tr>


</table>
<p>Compra la <a href="https://woofatture.com">Versione Premium!</a></p>
<div id="promo_premium">

</div>