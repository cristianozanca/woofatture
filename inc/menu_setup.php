<?php

/**
 *
 * @ Better separated voice to check quicker what every single value in add_menu_page is
 *
 */function woo_fattureincloud_setup_menu()
{
    $parent_slug = 'woocommerce';
    $page_title  = 'WooCommerce Fattureincloud Admin Page';
    $menu_title  = 'Fattureincloud';
    $capability  = 'manage_woocommerce';
    $menu_slug   = 'woo-fattureincloud';
    $function    = 'woo_fattureincloud_setup_page_display';

    add_submenu_page($parent_slug, $page_title, $menu_title, $capability, $menu_slug, $function);
}

function page_tabs( $current = 'ordine' ) {
	$tabs = array(
		'ordine'   => __( 'Ordine', 'woo-fattureincloud' ),
		'impostazioni'  => __( 'Impostazioni', 'woo-fattureincloud' ),
		'fatture' => __('Fatture', 'woo-fattureincloud'),
		'email' => __('Email', 'woo-fattureincloud')
	);
	$html = '<h2 class="nav-tab-wrapper">';
	foreach( $tabs as $tab => $name ){
		$class = ( $tab == $current ) ? 'nav-tab-active' : '';
		$html .= '<a class="nav-tab ' . $class . '" href="?page=woo-fattureincloud&tab=' . $tab . '">' . $name . '</a>';
	}
	$html .= '</h2>';
	echo $html;
}
