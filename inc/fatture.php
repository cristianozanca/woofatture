<?php

// Don't access this directly, please
if (!defined('ABSPATH')) exit;

?>

<table border="0" cellpadding="6">
    <tr>
            <td align="right">


            <form id="woo-fattureincloud-anno-fatture" method="POST">

	    	<?php wp_nonce_field(); ?>

            <label for="woo_fattureincloud_apiuid">Anno Fatture</label>

            <input type="number" name="woo-fattureincloud-anno-fatture" placeholder="anno"
                   value="<?php echo get_option('woo-fattureincloud-anno-fatture'); ?>">

            </td>
            <td>

                    <input type="submit" value="Save" class="button button-primary button-large">
            </td>
	</form>

        </tr>
</table>
    <div id="fatture-elenco">


<?php
$api_uid = get_option('api_uid_fattureincloud');
$api_key = get_option('api_key_fattureincloud');
$annofatture = get_option('woo-fattureincloud-anno-fatture');

$url = "https://api.fattureincloud.it:443/v1/fatture/lista";
$request = array(
	"api_uid" => $api_uid,
	"api_key" => $api_key,
	"anno" => $annofatture

);
$options = array(
	"http" => array(
		"header"  => "Content-type: text/json\r\n",
		"method"  => "POST",
		"content" => json_encode($request)
	),
);
$context  = stream_context_create($options);
$result = json_decode(file_get_contents($url, false, $context), true);

//echo "<pre>";
//print_r($result);
//echo "</pre>";

if (is_array($result))
{
	foreach ($result as $value)
    {

        if (is_array($value)) {

	        $count = 0;

            foreach ($value as $value2){

	            $count = $count + 1;

	            //echo $value2[$i];
                    /*
	               echo "<pre>";
                        print_r($value2);
                        echo "</pre>";
                    */

	            $idfattura = $value2['id'];

	            $url_dett = "https://api.fattureincloud.it:443/v1/fatture/dettagli";
	            $request_dett = array(
		            "api_uid" => $api_uid,
		            "api_key" => $api_key,
		            "id" => $idfattura

	            );
	            $options_dett = array(
		            "http" => array(
			            "header"  => "Content-type: text/json\r\n",
			            "method"  => "POST",
			            "content" => json_encode($request_dett)
		            ),
	            );
	            $context_dett  = stream_context_create($options_dett);
	            $result_dett = json_decode(file_get_contents($url_dett, false, $context_dett), true);
	           // print_r($result_dett);

	            if (is_array($result_dett))
	            {
	            	foreach ($result_dett as $value_dett) {

                        /*
	            	    echo "<pre>";
                        print_r($value_dett);
                        echo "</pre>";
                        */


	            	    if (!empty($value_dett['indirizzo_extra'])) {


                        echo "<form id=\"send_email_fattureincloud$idfattura\" method=\"POST\">";



			            print "<a href=\"https://secure.fattureincloud.it/invoices-view-".$value_dett['id']."\">Visualizza fattura</a><br>";
			            print "<b>".$value_dett['oggetto_visibile']."</b>";
			            print " Destinatario: ".$value_dett['nome']."";
			            print " | <b>importo iva inclusa</b> €".$value_dett['importo_totale']." <br> ";
			            print "<b>email</b> ".$value_dett['indirizzo_extra']."<br>";



			            $oggetto_email = $value_dett['oggetto_visibile'];

			            $email_destinatario = $value_dett['indirizzo_extra'];

			            $nome_cliente_fic = $value_dett['nome'];

			            $oggetto_ordine_fic = $value_dett['oggetto_visibile'];

			            echo "<input type=\"hidden\" value=\"$email_destinatario\" name=\"email_destinatario\" />";

			            echo "<button type=\"submit\" name=\"$idfattura\" value=\"$idfattura\" class=\"button button-primary\" >Invia Fattura via Email</button><hr>";

			                if (isset($_POST[$idfattura])) {

				                require (plugin_dir_path( __FILE__ ) . '/send_email_fattureincloud.php');

			                }

			                echo "</form><br>";
		                }



		            }


?>

<?php
	            }


	            if ($count == 20) {

		            print "numero massimo ( 20 ) di fatture visualizzabili raggiunto";
	                break;

	            }

             else {

	        }





        }

        }

    }


}

echo "</div>";


if (in_array("success", $result)) {


}

else  {

    	?><div id="message" class="notice notice-error is-dismissible">
		<p><b>Elenco Fatture non Scaricato: <?php

				echo $result['error'];

                ?></b>
	</div>
<?php
}
