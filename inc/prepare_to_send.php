<?php


require (plugin_dir_path( __FILE__ ) . '/send_to_fattureincloud.php');

require (plugin_dir_path( __FILE__ ) . '/error_cerca.php');


if (in_array("success", $fattureincloud_result)) {



	?>


	<div id="message" class="notice notice-success is-dismissible">

		<p><b>Creazione Riuscita!</b></p>
	</div>

	<script>
        jQuery("div#message").appendTo("div#top_fattureincloud");
	</script>

	<?php



}


elseif (!empty($valore_paese_iso))
{

	?>
	<div id="message" class="notice notice-error is-dismissible">
		<p><b>Creazione non Riuscita: Paese del cliente non disponibile</b></p>
	</div>

	<script>
        jQuery("div#message").appendTo("div#top_fattureincloud");
	</script>

	<?php

}


elseif (!empty($valore_iva))
{

	?>
	<div id="message" class="notice notice-error is-dismissible">
		<p><b>Creazione non Riuscita: Tipo Iva non abilitato</b></p>
	</div>

	<script>
        jQuery("div#message").appendTo("div#top_fattureincloud");
	</script>

	<?php

}




elseif (!empty($valore_api_uid))
{

	?>
	<div id="message" class="notice notice-error is-dismissible">
		<p><b>Creazione non Riuscita: Api Key mancanti o errate</b></p>
	</div>

	<script>
        jQuery("div#message").appendTo("div#top_fattureincloud");
	</script>

	<?php

}

else {

	?>
	<div id="message" class="notice notice-error is-dismissible">
		<p><b>Creazione non Riuscita: <?php

				echo $fattureincloud_result['error'];

				?> <br>Per verificare che le Impostazioni siano giuste <a href="admin.php?page=woo-fattureincloud&tab=impostazioni">clicca qui</a><br>
				Per maggiori informazioni <a href="https://woofatture.com/documentazione/">clicca qui</a></b></p>
	</div>

	<script>
        jQuery("div#message").appendTo("div#top_fattureincloud");
	</script>


	<?php

}