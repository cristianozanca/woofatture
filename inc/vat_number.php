<?php


/*
 *
 * valori nei campi del checkout
 *
*/

function billing_fields_woofc( $fields ) {


	$fields['billing_cod_fisc'] = array(
		'label'       => __('Codice Fiscale','woocommerce'),
		'placeholder' => __('Scrivere il Codice Fiscale','woocommerce'),
		'required'    => true,
		'class'       => array('piva-number-class form-row-wide' ),
		'clear'		  => true

	);

	$fields['billing_partita_iva'] = array(
		'label'       => __('Partita Iva','woocommerce'),
		'placeholder' => __('Scrivere il numero di Partita Iva','woocommerce'),
		'required'    => false,
		'clear'       => true,
		'class'       => array( 'form-row' ),

	);

	return $fields;
}

/*
 *
 * valori modificabili dell'ordine
 *
 * */



function admin_billing_field( $fields ) {

	$fields['cod_fisc'] = array(
		'label' => __('Codice Fiscale','woocommerce'),
		'wrapper_class' => 'form-field-wide',
		'show' => true,

	);
	$fields['partita_iva'] = array(
		'label' => __('Partita Iva','woocommerce'),
		'wrapper_class' => 'form-field-wide',
		'show' => true,

	);

	return $fields;
}

