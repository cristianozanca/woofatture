<?php

// Don't access this directly, please
if (!defined('ABSPATH')) exit;


$fattureincloud_url = "https://api.fattureincloud.it:443/v1/fatture/inviamail";

$api_uid = get_option('api_uid_fattureincloud');
$api_key = get_option('api_key_fattureincloud');





//print_r($lista_articoli);


$fattureincloud_request = array(

	"id" => $idfattura,
    "api_uid" => $api_uid,
    "api_key" => $api_key,
	"mail_mittente"=> "no-reply@fattureincloud.it",
    "mail_destinatario" => $email_destinatario,
	"oggetto" => $oggetto_email,
	"messaggio" => "Gentile " .$nome_cliente_fic. " ,<br>\n in allegato la fattura dell'".$oggetto_ordine_fic." in versione PDF.
                    <br><br>\n\nE' inoltre possibile  scaricarne una copia  premendo sul bottone sottostante<br><br>\n\n{{allegati}}<br><br>\n\nCordiali saluti",
	"includi_documento" => true,
	"invia_fa" => true,
	"includi_allegato" => true,
	"invia_copia" => true,
	"allega_pdf" => true

);


$fattureincloud_options = array(
    "http" => array(
        "header" => "Content-type: text/json\r\n",
        "method" => "POST",
        "content" => json_encode($fattureincloud_request)
    ),
);
$fattureincloud_context = stream_context_create($fattureincloud_options);
$fattureincloud_result = json_decode(file_get_contents($fattureincloud_url, false, $fattureincloud_context), true);
//print_r($fattureincloud_result);

if (in_array("success", $fattureincloud_result)) {

?>
<div id="message" class="notice notice-success is-dismissible">
	<p><b>Invio Riuscito!</b></p>
</div>
	<?php
}

else  {

    	?><div id="message" class="notice notice-error is-dismissible">
		<p><b>Invio non Riuscito: <?php

				echo $fattureincloud_result['error'];

                ?></b>
	</div>
<?php
}






















