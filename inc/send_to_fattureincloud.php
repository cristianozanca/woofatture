<?php

// Don't access this directly, please
if (!defined('ABSPATH')) exit;


$fattureincloud_url = "https://api.fattureincloud.it:443/v1/fatture/nuovo";

$api_uid = get_option('api_uid_fattureincloud');
$api_key = get_option('api_key_fattureincloud');


$lista_articoli = array();


//$aggiungi_shipping = array();

//$spedizione_netta = $order_data['shipping_total'] - ($order_data['shipping_total'] - round(($order_data['shipping_total'] / 122) * 100, 2));
$spedizione_lorda = $order_data['shipping_total'] + $order_data['shipping_tax'] ;
$spedizione_netta = $spedizione_lorda - $order_data['shipping_tax'];

$codice_iva = '';


foreach ($order->get_items() as $item_key => $item_values):

    $item_data = $item_values->get_data();


    $line_total = $item_data['total'];


    $product_id = $item_values->get_product_id(); // the Product id
    $wc_product = $item_values->get_product(); // the WC_Product object
    ## Access Order Items data properties (in an array of values) ##
    $item_data = $item_values->get_data();
    $_product = wc_get_product($product_id);

    //$tax_rates = WC_Tax::get_rates($_product->get_tax_class());
	$tax_rates = WC_Tax::get_base_tax_rates( $_product->get_tax_class( true ) );

    $tax_rate = reset($tax_rates);

    if ($tax_rate['rate'] == 22) {

        $codice_iva = 0;


    } elseif ($tax_rate['rate'] == 0) {

        $codice_iva = 6;


	} elseif ($tax_rate['rate'] == 4) {

		$codice_iva = '';

	} elseif ($tax_rate['rate'] == 10) {

	    $codice_iva = '';

    }

   // $prezzo_singolo_prodotto = (round($item_data['total'], 2) / $item_data['quantity']);

	$prezzo_singolo_prodotto = ((round($item_data['total'], 2)+$item_data['total_tax']) / $item_data['quantity']);
	$ivatosiono = true;


    $lista_articoli[] = array(
        "nome" => $item_data['name'],
        "quantita" => $item_data['quantity'],
        "cod_iva" => $codice_iva,
        "prezzo_netto" => $prezzo_singolo_prodotto,
        "prezzo_lordo" => $prezzo_singolo_prodotto


    );

endforeach;


if ($order_data['shipping_total'] > 0) {

    $lista_articoli[] =

        array(

            "nome" => "Spese di Spedizione",
            "quantita" => 1,
            "cod_iva" => 0,
            "prezzo_netto" => $spedizione_netta,
            "prezzo_lordo" => $spedizione_lorda


        );

}


//print_r($lista_articoli);


$fattureincloud_request = array(

    "api_uid" => $api_uid,
    "api_key" => $api_key,
    "nome" => $order_billing_first_name . " " . $order_billing_last_name . " " . $order_billing_company,
    "indirizzo_via" => $order_billing_address_1,
    "indirizzo_cap" => $order_billing_postcode,
    "indirizzo_citta" => $order_billing_city,
    "paese_iso" => $order_billing_country,
    "prezzi_ivati" => $ivatosiono ,
    "indirizzo_extra" => $order_billing_email,
    "piva" => $order_billing_partiva,
    "cf" => $order_billing_codfis,
    "oggetto_visibile" => "Ordine numero ".$id_ordine_scelto,
	"mostra_info_pagamento" => true,
    "metodo_pagamento" => "Metodo scelto". $order_billing_method,
    "lista_articoli" => $lista_articoli,
    "lista_pagamenti" => [
        array(
            "data_scadenza" => $order_data['date_created']->date('d/m/Y'),
            "importo" => 'auto',
            "metodo" => "Metodo scelto".$order_data['payment_method'],
            "data_saldo" => $order_data['date_created']->date('d/m/Y'),

        )
    ]
);


$fattureincloud_options = array(
    "http" => array(
        "header" => "Content-type: text/json\r\n",
        "method" => "POST",
        "content" => json_encode($fattureincloud_request)
    ),
);
$fattureincloud_context = stream_context_create($fattureincloud_options);
$fattureincloud_result = json_decode(file_get_contents($fattureincloud_url, false, $fattureincloud_context), true);
// print_r($fattureincloud_result);
