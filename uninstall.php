<?php
// If uninstall not called from WordPress exit
if( !defined( 'WP_UNINSTALL_PLUGIN' ) )
	exit ();

// Delete option from options table
delete_option( 'api_key_fattureincloud' );

delete_option( 'api_uid_fattureincloud' );

delete_option( 'woo_fattureincloud_order_id' );

delete_option( 'fattureincloud_auto_save' );

delete_option( 'woo-fattureincloud-anno-fatture' );

delete_option( 'fattureincloud_partiva_codfisc' );


//remove any additional options and custom tables
