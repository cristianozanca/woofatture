=== WooCommerce Fattureincloud ===
Contributors: cristianozanca
Tags: fattureincloud, fatture, cloud, woocommerce, bill
Requires at least: 4.6
Tested up to: 4.9
Requires PHP: 5.6
Stable tag: 1.5.2
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html


WooCommerce Fattureincloud

== Description ==

The WooCommerce Fattureincloud plugin allows you to transform the orders received in your online store made with WooCommerce in Invoices on Fattureincloud.it

Here you can see how it works:

[youtube https://www.youtube.com/watch?v=UPKfQjEzjOw?rel=0]




* The **Free Version** manages the last 10 orders and IVA at 0 and 22%
* The **Premium Version** manages all orders and IVA at 0, 22%, 4% and 10%

[More detailed info here - Qui la Documentazione](https://woofatture.com/documentazione/)

Try Fattureincloud for free at [this address](https://www.fattureincloud.it/service/form/form-registrazione/)

How does it work? = Select the order number from the drop-down menu, check in the preview that it is the right one and then send it to Fattureincloud.it

Then you can open the bill at fattureincloud and you can also send it via email to the customer

The WooCommerce plugin Fattureincloud requires the UID API and KEY API
that can be found at [this address](https://secure.fattureincloud.it/api)


== Changelog ==

= 1.5.2 =
* WC 3.4 compatibility

= 1.5.1 =
* modalità pagamento, testo email, bottone creazione, compatibilità plugin esterno

= 1.5.0 =
* bill list & email send

= 1.0.2 =
* mix fixes

= 1.0.1 =
* msg error

= 1.0.0 =
* layout vari fix

= 0.5.4 =
* fix name + missing orders

= 0.5.3 =
* file missing fix

= 0.5.2 =
* email - tabs added

= 0.5.0 =
* insert product no tax included

= 0.4.3 =
* no order fix

= 0.4.2 =
* role shop manager

= 0.4.1 =
* fixes

= 0.4.0 =
* numero d'ordine

= 0.3.2 =
* descrizione e CF obbligatorio

= 0.3.1 =
* più descrizione

= 0.3.0 =
* campi codice fiscale, partita iva

= 0.2.5 =
* segnalazione errori autosend e segnalazioni varie

= 0.2.4 =
* aggiunta funzione sperimentale invio automatico fatture

= 0.2.3 =
* prezzi senza iva inclusa

= 0.2.2 =
* Description

= 0.2.0 =
* Fixes Iva

= 0.1.0 =
* Initial release

== Upgrade Notice ==
* Initial release

== Installation ==

This section describes how to install the plugin and get it working.

1. Unzip `woo-fattureincloud.zip`
2. Upload the `woo-fattureincloud` directory (not its contents, the whole directory) to `/wp-content/plugins/`
3. Activate the plugin through the `Plugins` menu in WordPress

== Frequently Asked Questions ==

= I prezzi li posso mettere sia con iva inclusa ed iva esclusa? =

No è necessario impostare l'opzione "No, inserirò prezzi al netto di imposta"
Nel negozio online i prezzi possono essere comunque mostrati iva inclusa

== Screenshots ==

1. Invio Riuscito!
2. Quando l'invio non riesce appare la motivazione
3. Ecco le fatture inviate

== Credits ==
* Huge thanks to LoicTheAztec, Pascal Knecht