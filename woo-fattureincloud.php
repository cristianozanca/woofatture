<?php

/*
 * Plugin Name: WooCommerce Fattureincloud
 * Plugin URI:  https://woofatture.com/
 * Description: WooCommerce Fattureincloud integration
 * Version:     1.5.3
 * Contributors: cristianozanca
 * Author:      Cristiano Zanca
 * Author URI:  https://zanca.it
 * License:     GPL2
 * License URI: https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain: woo-fattureincloud
 * Domain Path: /languages
 * WC requires at least: 3.1.0
 * WC tested up to: 3.4.0
*/

function woo_fattureincloud_textdomain()
{
    load_plugin_textdomain('woo-fattureincloud', FALSE, basename(dirname(__FILE__)) . '/languages');
}

add_action('plugins_loaded', 'woo_fattureincloud_textdomain');


if (!class_exists('woo_fattureincloud')) : {
    class woo_fattureincloud
    {

        public function __construct()
        {

            if (!defined('ABSPATH')) {

                exit; // Exit if accessed directly

            }


            include_once plugin_dir_path(__FILE__) . 'inc/menu_setup.php';

            include_once plugin_dir_path(__FILE__) . 'inc/setup_page_display.php';


            add_action('admin_enqueue_scripts', array($this, 'register_woo_fattureincloud_styles_and_scripts'));

            add_action('admin_menu', 'woo_fattureincloud_setup_menu');




            add_action( 'woocommerce_order_status_completed',array(&$this,'fattureincloud_order_completed'),10,1 );

            if ( 1 == get_option('fattureincloud_partiva_codfisc') ) {

                include_once plugin_dir_path(__FILE__) . 'inc/vat_number.php';

	            add_filter( 'woocommerce_admin_billing_fields' ,  'admin_billing_field' );

	            add_filter( 'woocommerce_billing_fields' , 'billing_fields_woofc', 10, 1);
            }

        }


        function fattureincloud_order_completed($order_id) {

            if ( 1 == get_option('fattureincloud_auto_save') )
            {         //   error_log("$order_id set to COMPLETED", 0);

                update_option('woo_fattureincloud_order_id', $order_id );

                require (plugin_dir_path( __FILE__ ) . 'inc/setup-file.php');
                require (plugin_dir_path( __FILE__ ) . 'inc/send_to_fattureincloud.php');

                if (!in_array("success", $fattureincloud_result)) {

                    update_option('fattureincloud_autosent_id_fallito', $order_id);

                }

            }

        }


        /**
         *
         * Custom stylesheet to load image and js scripts only on backend page
         *
         */
        function register_woo_fattureincloud_styles_and_scripts($hook)
        {

            $current_screen = get_current_screen();

            if (strpos($current_screen->base, 'woo-fattureincloud') === false) {
                return;
            } /*
                        elseif( $hook != 'woocommerce_page_digthis-woocommerce-fattureincloud' ){
                            return;
                        }
                        */

            else {

                wp_enqueue_style('boot_css', plugins_url('assets/css/woo_fattureincloud.css', __FILE__));


            }


        }


    }

}

//Creates a new instance
    new woo_fattureincloud;

endif;
